<?php

abstract class Conta
{
   private $tipoDeConta;
   public function getTipoDaConta(){
	   return $this-> tipoDaConta;
   }
   public function setTipoDaConta ( string $tipoDaConta){
       $this-> tipoDaConta = $tipoDaConta;
   }	   
   public $agencia;
   public $conta;
   protected $saldo = 0;
   
   public function ImprimeExtrato()
   {
	   
	   echo ' conta ' . $this-> tipoDeConta . ' agencia ' . $this ->agencia . ' conta ' .  $this->conta . ' com Saldo de; ' . $this-> calculaSaldo();
   }
   
   public function deposito(float $valor )  
   {
	   if($valor > 0 )
	   {   
	//$this->saldo = $this->saldo + $valor;   
	  $this-> saldo +=$valor;
	  echo "Deposito afetuado com sucesso!!! <br>";
   }   
   else
   {
	   echo "Valor de depósito invalido!!! <br>";
   }
}   

   public function saque(float $valor)
   {     
	//$this-> saldo = $this-> saldo - $valor;  
	if($this-> saldo >- $valor)
	{		
	   $this-> saldo -= $valor;
	   echo "Saque realizado com sucesso <br>";
	}
	else
	{
		echo "Saldo insuficiente!!! <br>";
    }
}      
	abstract public function calculaSaldo();
	
}	

class poupanca extends Conta
{
	public $reajuste;
	
	public function __construct (string $agencia, string $conta, Float $reajuste)
	{
		$this-> setTipoDaConta('Poupança');
		$this-> reajuste = $reajuste;
		$this-> agencia = $agencia;
		$this-> conta = $conta;
	} 
	 public function calculaSaldo()
	 { 
	    return $this->saldo + ($this-> saldo * $this-> reajuste/100);
	 }
	 
}

class Especial extends Conta
{

	public $especial;
	
	public function __construct (string $agencia, string $conta, float $especial)
	{
		$this-> setTipoDaConta ('Especial');
		$this-> especial = $especial;
		$this-> agencia = $agencia;
		$this-> conta = $conta; 
	}
	 public function calculaSaldo()
	 { 
	    return $this->saldo + $this-> especial;
	 }
	 
}

$ctaPoup = new Poupanca('0002-7', '85588-88', 0.54);
//$ctaPoup-> saldo - -1500; 
//Não pode acessaor atributo protegido
$ctaPoup-> deposito  (-1500);
$ctaPoup-> saque(3000);
$ctaPoup->imprimeExtrato();

echo '<br>';

$ctaEspecial = new Especial ('0055-2', '75588-42', 2300);
$ctaEspecial-> deposito (1500);
$ctaEspecial-> imprimeExtrato();

?>